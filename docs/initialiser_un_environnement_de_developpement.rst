Initialiser un environnement de développement
=============================================

Voici les étapes pour initialiser un environnement de développement.
Il peut être installé sur n'importe quelle plateforme.

TODO : Valider la possibilité d'installer l'appli sur Windows, Mac ou autres

Prérequis
---------

1. Installer les composants suivants :
    - python 2.7 + pip
    - git
    - sqlite
2. Configurer git


Récupération des sources
------------------------

1. Se positionner dans un répertoire (`<REP_PROJET>`) dans lequel sera installé l'application
2. Descendre les sources : ``git clone https://apy@bitbucket.org/apy/montchocolat.git``
3. Vérifier que le répertoire montchocolat a été créé

.. HINT::
    Toutes les actions suivantes devront être lancées à partir du répertoire `<REP_PROJET>`   

Configuration de l'environnement virtuel
----------------------------------------

1. Installer virtualenv + vitualenvwrapper
2. Configurer virtualenvwrapper_
3. Créer un environnement virtuel "montchocolat" : ``mkvirtualenv -p $(which python2.7) montchocolat``
4. Si ce n'est pas déjà fait, se positionner sur ce nouvel environnement ``workon montchocolat``
5. Installer les dependances à l'aide du fichier dependencies.txt : ``pip install -r montchocolat/dependencies.txt``

Création du fichier "montchocolat/my_settings.py"
--------------------------------------------------
Créer le fichier my_setteings.py : ``echo "MC_ROOT_PATH='$(pwd)'\n" > montchocolat/montchocolat/my_settings.py``

Import des photos et chargement des données dans la base
--------------------------------------------------------

1. Initialiser la base de données : ``python montchocolat/manage.py syncdb`` (Se créer son propre compte superuser)
2. Télécharger le fichier 20130425_purge.tar présent sur bitbucket dans le répertoire `<REP_PROJET>`
3. Extraire l'archive : ``tar xvf 20130425_purge.tar``
4. Charger les données dans la base : ``python montchocolat/manage.py loaddata 20130425_purge/data.json`` (Résultat attendu : `Installed 111 object(s) from 1 fixture(s)`)
5. Créer le répertoire de photos : ``mkdir -p montchocolat_media``
6. Extraire les photos : ``tar xvf 20130425_purge/photos_dump.tar -C montchocolat_media``
7. (Optionnel) supprimer les fichiers résiduels : ``rm 20130425_purge/photos_dump.tar 20130425_purge/data.json && rmdir 20130425_purge``

Démarrage de l'environnement de développement
---------------------------------------------

1. `python montchocolat/manage.py runserver`

.. _virtualenvwrapper: http://virtualenvwrapper.readthedocs.org/en/latest/
