from mc_catalogue.models import Catalogue, Product
from django.shortcuts import render, get_object_or_404
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

#############################
######### CATALOGUE #########
#############################

def show_chocolats_catalogue(request):
    """
    Show "Chocolats" catalogue
    """
    return __show_catalogue(request, "Chocolats")
    
def show_patisserie_catalogue(request):
    """
    Show "Patisserie" catalogue
    """
    return __show_catalogue(request, "Patisserie")
    
def __show_catalogue(request, catalogue_name):
    """
    From catalogue_name parameter, retrieve the catalogue and show the directs
    subcatalogue
    """
    catalogue = get_object_or_404(Catalogue, name = catalogue_name)
    subcatalogues_list = list(
        Catalogue.objects.filter(is_subcatalogue_of = catalogue)
    )
    logger.debug("subcatalogues_list : {}".format(subcatalogues_list))
    # FIXME Array size must be divisible by 2
    __array_size_divisible_by(2, subcatalogues_list)
    return render(
        request,
        "frontend/catalogue.html",
        {"catalogue" : catalogue, "subcatalogues_list" : subcatalogues_list}
    )

#############################
########## PRODUCT ##########
#############################

def show_products(request, catalogue_id):
    """
    From catalogue_id parameter, retrieve all products
    """
    catalogue = get_object_or_404(Catalogue, pk = catalogue_id)
    # Simple Case
    if len(catalogue.subcatalogues_list.all()) == 0:
        products_list = list(catalogue.products_list.filter(active=True))
        for product in products_list:
	    if product.name.startswith("_"):
                product.name = ""
        logger.debug("Simple Case - products_list : {}".format(products_list))
        # FIXME Array size must be divisible by 4
        __array_size_divisible_by(4, products_list)
        return render(
            request,
            "frontend/simple_products.html",
            {"catalogue" : catalogue, "products_list" : products_list }
        )
        
    # Complex Case
    subcatalogues_list = list()
    catalogue.get_all_subcatalogues_list(subcatalogues_list)
    
    catalogues_products_map = dict(
        map(
            lambda x : [x, list(x.products_list.filter(active=True))],
            subcatalogues_list
        )
    )
    
    for products_list in catalogues_products_map.values():
        for product in products_list:
            if product.name.startswith("_"):
                product.name = ""
 
    logger.debug("Complex Case - catalogues_products_map : {}".format(
        catalogues_products_map
    ))
    
    # FIXME Array size must be divisible by 4
    for products_list in catalogues_products_map.values():
        __array_size_divisible_by(4, products_list)
        
    return render(
        request,
        "frontend/complex_products.html",
        {
            "catalogue" : catalogue,
            "catalogues_products_map" : catalogues_products_map
        }
    )

def __array_size_divisible_by(value, array):
    """
    Add None into the array until its size is divisible by "value" parameter
    """
    diff_nb = len(array) % value
    if diff_nb == 0:
        return
    nb = value - diff_nb
    array.extend([None] * nb)

#############################
########## IMAGE ############
#############################

def show_product(request, product_id):
    """
    Show one product information
    """
    product = get_object_or_404(Product, pk = product_id)
    return render(
        request,
        "frontend/product.html",
        {"product" : product}
    )
