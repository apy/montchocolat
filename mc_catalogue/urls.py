from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView

urlpatterns = patterns('mc_catalogue.views',
    url(r'^chocolats/?$', "show_chocolats_catalogue", name = "chocolats_catalogue"),
    url(r'^patisserie/?$', "show_patisserie_catalogue", name = "patisserie_catalogue"),
    url(r'^products/(?P<catalogue_id>\d+)$', "show_products", name = "products"),
    url(r'^product/(?P<product_id>\d+)$', "show_product", name = "product"),
)
