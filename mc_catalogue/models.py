from django.db import models
from sorl.thumbnail import ImageField


class Catalogue(models.Model):
    '''Describe a catalogue object'''

    name = models.CharField(max_length=20)
    libelle = models.CharField(max_length=80)
    
    is_subcatalogue_of = models.ForeignKey(
        "Catalogue",
        related_name="subcatalogues_list",
        blank = True,
        null = True
    )
    
    picture = ImageField(upload_to="catalogues", max_length = 512, blank = True)
    
    def __unicode__(self):
        return self.name

    def get_all_subcatalogues_list(self, subcatalogues_list):
        """
        Append into subcatalogues_list parameter the list of all subcatalogues 
        """
        tmp_subcatalogues_list = self.subcatalogues_list.all()
        if len(tmp_subcatalogues_list) == 0:
            subcatalogues_list.append(self)
        else:
            for tmp_subcatalogue in tmp_subcatalogues_list:
                tmp_subcatalogue.get_all_subcatalogues_list(subcatalogues_list)
                
                
class Product(models.Model):
    '''A product from a catalogue'''

    name = models.CharField(max_length=80)
    catalogue = models.ForeignKey("Catalogue", related_name = "products_list")
    picture = ImageField(upload_to="products", max_length = 512, blank = True)
    ingredients = models.CharField(max_length=2000, blank = True)
    active = models.BooleanField(default = False)

    def __unicode__(self):
        return self.name

