from django.contrib import admin
from mc_catalogue.models import Catalogue, Product

class CatalogueAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_subcatalogue_of', 'picture')
    search_fields = ('name', 'is_subcatalogue_of', 'picture')

class ProductAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'catalogue', 'ingredients', 'picture', 'active')
    search_fields = ('name', 'catalogue', 'ingredients', 'picture')
    list_editable = ('active',)

admin.site.register(Product, ProductAdmin)
admin.site.register(Catalogue, CatalogueAdmin)
