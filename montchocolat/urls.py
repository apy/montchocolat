from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='frontend/index.html'), name = "accueil"),
    url(r'^macarons$', TemplateView.as_view(template_name= 'frontend/macarons.html'), name = "macarons"),
    url(r'^acces$', TemplateView.as_view(template_name= 'frontend/acces.html'), name = "acces"),
    url(r'^contact$', TemplateView.as_view(template_name= 'frontend/contact.html'), name = "contact"),
    url(r'^404$', TemplateView.as_view(template_name= '404.html'), name = "404"),
    url(r'^500$', TemplateView.as_view(template_name= '500.html'), name = "500"),
    url(r'^catalogue/', include('mc_catalogue.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^medias/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
)
