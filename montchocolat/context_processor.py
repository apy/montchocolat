"""
Add automatically properties into the context
"""
from random import randrange

FORCE_CACHE_ID = str(randrange(99999))

def force_cache_id(request):
    return {'FORCE_CACHE_ID': FORCE_CACHE_ID}
