SLIDES_ENTRY_POINT = "#slides";

/**
Version améliorée du modulo
*/
Number.prototype.mod = function(n) {
return ((this%n)+n)%n;
}

/**
Récupère les id de toutes les images incluses dans #slides
*/
function get_slides() {
    var slides_list = Array()
    if ($(SLIDES_ENTRY_POINT).length == 0) {
        return slides_list;
    }
    $(SLIDES_ENTRY_POINT).children().each(function(img) {
        slides_list.push($(this).attr("id"));
    });
    return slides_list;
}

/**
Lance la fonction slide permettant de changer automatiquement les images
*/
function slide() {
    var slides_list = get_slides();
    var slides_size = slides_list.length;
    if (slides_size == 0) {
        return;
    }
    var i = 0;
    
    function display_slide() {
        i++;
        var previous_slide = (i-1).mod(slides_size);
        var next_slide = i.mod(slides_size);
        $("#" + slides_list[previous_slide]).fadeOut(2000);
        $("#" + slides_list[next_slide]).fadeIn(2000);
    }
    
    setInterval(display_slide, 5000);
}

/**
Point d'entrée d'exécution du javascript
*/
$(document).ready(function() {
    $(function() {
        slide();
    });
});
